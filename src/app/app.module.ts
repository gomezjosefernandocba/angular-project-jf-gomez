import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { CanActivateViaAuthGuard } from './guards/userlogin.guard';


import { SharedModule } from './shared/shared.module';
import { FeaturesModule } from './features/features.module';
import { PagesModule } from './pages/pages.module';

import { StoreModule } from '@ngrx/store';
import { UserEffects } from './features/store/user.effects';
import * as fromUsersReducer from './features/store/user.reducers';

import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools'

import { environment } from 'src/environments/environment';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    StoreModule.forRoot({ users: fromUsersReducer.reducer }),
    EffectsModule.forRoot([UserEffects]),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
    PagesModule,
    FeaturesModule,
    NgbModule,
  ],
  providers: [CanActivateViaAuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
