import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate } from '@angular/router';
import { UserLoginService } from '../pages/services/userlogin.service';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }
}

@Injectable()
export class CanActivateViaAuthGuard implements CanActivate {

    constructor(private authService: UserLoginService, private router: Router) { }

    canActivate() {
        if (!this.authService.isLoggedIn()) {
            this.router.navigate(['/']);
            return false;
        }
        return true;
    }
}
