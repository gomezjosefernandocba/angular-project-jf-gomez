import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs';
import { UserLogin } from '../../models/userlogin.model';
import { UserLoginService } from '../../services/userlogin.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

  user: UserLogin = {userlogin: '',password: '', token: ''};
    constructor(private UserLoginService: UserLoginService, public router: Router) {
  }

  ngOnInit() {
   
  }

  onSubmit(): void {
    this.UserLoginService.login(this.user.userlogin,this.user.password)
    .subscribe(res => {
        localStorage.setItem('token', JSON.stringify(res));
        this.router.navigateByUrl('/user');
    }, error => {
      alert(JSON.stringify(error));
    }
    );
  }

}
