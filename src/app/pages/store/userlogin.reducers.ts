/*
import { UserLogin } from '../models/userlogin.model';
import { Action, createReducer, on } from '@ngrx/store';
import  * as AuthenticationActions from '../store/userlogin.actions';

export interface State {
  isAuthenticated: boolean;
  userLogin: UserLogin | null;
  errorMessage: string | null;
}

export const initialState: State = {
  isAuthenticated: localStorage.getItem('token')!==null,
  userLogin: {
          token: localStorage.getItem('token'),
          userlogin: localStorage.getItem('userlogin')
        },
  errorMessage: null
};

const userReducer = createReducer(
  initialState,
  on(AuthenticationActions.LoginSuccess, (state,{userLogin}) => { return {
    ...state,
    isAuthenticated: true,
    userLogin: {
      token: action.payload.token/*action.payload.token,
      userlogin: action.payload.userlogin
    },
    errorMessage: null
  }};)
  );


/*
export function reducer(state = initialState, action: AuthenticationActions): State {
  switch (action.type) {
    case AuthenticationActionTypes.LOGIN_SUCCESS: {
      return {
        ...state,
        isAuthenticated: true,
        userLogin: {
          token: action.payload.token,
          userlogin: action.payload.userlogin
        },
        errorMessage: null
      };
    }
    case AuthenticationActionTypes.LOGIN_FAILURE: {
      return {
        ...state,
        errorMessage: '¡Credenciales Invalidas!'
      };
    }
    case AuthenticationActionTypes.LOGOUT: {
      return initialState;
    }
    default: {
      return state;
    }
  }
}
*/