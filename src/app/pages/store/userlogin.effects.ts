/*
import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Router } from '@angular/router';
import { Actions, Effect, createEffect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map, switchMap, catchError, tap } from 'rxjs/operators';
import { UserLoginService } from '../services/userlogin.service';
import {
  AuthenticationActionTypes,
  Login, LoginSuccess, LoginFailure,Logout
} from '../store/userlogin.actions';


@Injectable()
export class AuthenticationEffects {
  constructor(
    private actions: Actions,
    private authenticationService: UserLoginService,
    private router: Router,
  ) {}


@Effect()
Login: Observable<any> = this.actions.pipe(
  ofType(AuthenticationActionTypes.LOGIN)
  ,map((action: Login) => action.payload)
  ,switchMap(payload => {
      return this.authenticationService.login(payload.userlogin, payload.password).pipe(
        map((user) => {
          console.log(user);
          return new LoginSuccess({token: user.token, email: payload.email});
        }))
        ,catchError((error) => {
          console.log(error);
          return of(new LoginFailure({ error: error }));
        })
  }));
  
  @Effect({ dispatch: false })
  LoginSuccess: Observable<any> = this.actions.pipe(
    ofType(AuthenticationActionTypes.LOGIN_SUCCESS),
    tap((user) => {
      localStorage.setItem('token', user.payload.token);
      localStorage.setItem('userlogin', user.payload.userlogin);
      this.router.navigateByUrl('/user');
    })
  );

  @Effect({ dispatch: false })
  LoginFailure: Observable<any> = this.actions.pipe(
    ofType(AuthenticationActionTypes.LOGIN_FAILURE)
  );
  

  @Effect({ dispatch: false })
  public Logout: Observable<any> = this.actions.pipe(
    ofType(AuthenticationActionTypes.LOGOUT),
    tap((user) => {
      localStorage.removeItem('token');
      localStorage.removeItem('userlogin');
      this.router.navigateByUrl('/login');
    })
  );
 */
