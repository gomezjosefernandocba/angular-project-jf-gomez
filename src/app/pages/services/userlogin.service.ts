import { Injectable } from '@angular/core';
import { Observable } from "rxjs";
import { UserLogin } from '../models/userlogin.model';

@Injectable({
  providedIn: 'root'
})
export class UserLoginService {

  testUser: UserLogin = {userlogin: 'test',password: 'Ayi+2020', token: 'sampleToken'};

  constructor() { }

  getToken(): string {
    return localStorage.getItem('token');
  }

  isLoggedIn() {
    const token = this.getToken();
    return token != null;
  }
  
  login(userlogin: string, password: string): Observable<any> {
    return new Observable((observer) => {
      if (userlogin === this.testUser.userlogin && password === this.testUser.password) {
        observer.next({userlogin: this.testUser.userlogin, token: this.testUser.token});
      } else {
        observer.error({error: 'Credenciales Invalidas'});
      }
      observer.complete();
    });
  }

}