import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent}  from './pages/components/login/login.component';
import {HomeComponent}  from './pages/components/home/home.component';
import {UserListComponent}  from './features/components/user-list/user-list.component';
import { UserFormComponent } from './features/components/user-form/user-form.component';
import { CanActivateViaAuthGuard } from './guards/userlogin.guard';

const routes: Routes = [
  { path: 'home',  component: HomeComponent},
  { path: 'login', component: LoginComponent },
  { path: 'user', component: UserListComponent, canActivate: [CanActivateViaAuthGuard] },
  { path: 'user-form',component: UserFormComponent},
  { path: '', pathMatch: 'full', redirectTo: 'home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
