import { Adress } from './adress.model';

export interface User {
    id?: number;
    name: string;
    username: string;
    email: string;
    phone: string;
    website?: string;
    adress?: Adress;
    delete?: boolean;
}