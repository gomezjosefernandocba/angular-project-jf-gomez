import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder,Validators, Form } from '@angular/forms';
import { Store } from '@ngrx/store';
import * as fromActionsUser from '../../store/user.actions';
import { UsersState } from '../../store/user.reducers';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {
 /*  
  public userNew: User = {
    name: '',
    username:'',
    email: '',
    phone: '',
  };
  */
  userForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private store: Store<{ users: UsersState }>) { }

  ngOnInit(): void {

    this.userForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      name: ['', [Validators.required]],
      email: ['', [Validators.required]],
      phone: ['', [Validators.required]]
    });

  }

  onSubmit(){
    if(this.userForm.valid){
      /*this.userNew.username = this.userForm.get('username').value;
      this.userNew.name = this.userForm.get('name').value;
      this.userNew.email = this.userForm.get('email').value;
      this.userNew.phone = this.userForm.get('phone').value;*/
      this.createUser();
      alert('El usuario ' + this.userForm.get('name').value + ' fue creado con éxito.');
      this.limpiarCampos();
    } else {
      alert('No se pudo registrar el usuario.')
    }
  }

  public createUser(){
    this.store.dispatch(fromActionsUser.CreateData({user:this.userForm.value}));
  }

  public limpiarCampos(){
    this.userForm.reset({
      username: '',
      name: '',
      email: '',
      telefono: ''}
    );
  }

}
