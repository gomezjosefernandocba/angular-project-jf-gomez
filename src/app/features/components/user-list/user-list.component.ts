import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from '../../models/user.model';
import { Subscription, from } from 'rxjs';
import { Store } from '@ngrx/store';

import { UsersState } from '../../store/user.reducers';
import * as fromActionsUsers from '../../store/user.actions';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})

export class UserListComponent implements OnInit , OnDestroy {

  userList: User[] =[];
  private subscriptions: Subscription[] = [];
  public isLoading = true;

  constructor(
    private store: Store<{ users: UsersState }>
  ) { }

  ngOnInit(): void {
    this.store.dispatch(fromActionsUsers.FetchPending());
    this.store.subscribe(( {users} ) => {
      this.isLoading = users.pending;
      if(users.data && users.data.length) {
        this.formatData(users.data);
      }
    })

  }

  formatData(users: any) {
    this.userList = users.map(user => {
      return { 
        id:       user.id,
        name:     user.name,
        username: user.username,
        email:     user.email,
        phone:    user.phone,
        website:  user.website,
        address: {
          city:user['address'].city,
          street:user['address'].street,
          suite:user['address'].suite,
          zipcode:user['address'].zipcode,
        }, 
        delete: false
      }
    });
  }


  ngOnDestroy(): void{
    this.subscriptions.forEach( (s: Subscription) => s.unsubscribe() );
  }

}
