import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  apiUrl:string = `${environment.base_url_api}/users`;

  constructor(private http: HttpClient) { }

  getUserList() {
    return this.http.get(this.apiUrl);
  }

  getUserById(id: number) {
    const url = `${this.apiUrl}/${id}`;
  }

}
